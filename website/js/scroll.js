function currentYPosition()
{
	// Firefox, Chrome, Opera, Safari
	if (self.pageYOffset) 
		return self.pageYOffset;

	// Internet Explorer 6 - standards mode
	if (document.documentElement && document.documentElement.scrollTop)
		return document.documentElement.scrollTop;

	// Internet Explorer 6, 7 and 8
	if (document.body.scrollTop)
		return document.body.scrollTop;
	
	return 0;
}


function elmYPosition(eID)
{
	var elm = document.getElementById(eID);
	var y = elm.offsetTop;
	var node = elm;
	while (node.offsetParent && node.offsetParent != document.body)
	{
		node = node.offsetParent;
		y += node.offsetTop;
	} 

	return y;
}

function smoothScroll(eID)
{
	var maxSpeed = 20;

	var startY = currentYPosition();
	var stopY = elmYPosition(eID);
	var distance = stopY > startY ? stopY - startY : startY - stopY;

	if (distance < 100)
	{
		scrollTo(0, stopY);
		return;
	}

	var speed = Math.round(distance / 100);

	if (speed >= maxSpeed)
		speed = maxSpeed;

	var step = Math.round(distance / 25);
	var leapY = stopY > startY ? startY + step : startY - step;
	var timer = 0;

	if (stopY > startY)
	{
		for ( var i=startY; i<stopY; i+=step )
		{
			setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
			leapY += step; 

			if (leapY > stopY)
				leapY = stopY; timer++;
		}

		return;
	}
	
	for ( var i=startY; i>stopY; i-=step )
	{
		setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
		leapY -= step; 

		if (leapY < stopY)
			leapY = stopY; timer++;
	}
}

function  windowScroll(e)
{
	var e = window.event || e; // old IE support
	var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
	
	//down
	if(delta < 0)
		smoothScroll('about');
	//up
	else
		smoothScroll('home');
}

// IE9, Chrome, Safari, Opera
document.addEventListener("mousewheel", windowScroll, false);
// Firefox
document.addEventListener("DOMMouseScroll", windowScroll, false);