var helper = 
{
	extractDomain: function(url) 
	{
		var parser = document.createElement('a');
		parser.href = url;
	
		return parser.hostname;
	},
	
	extractElement: function(list, predicate)
	{
		for (var i = 0; i < list.length; i++)
		{
			if(predicate(list[i]))
				return list[i];
		}
	
		return null;
	}
};

function spawnModal(titleText, contentText, type="", okCancel=false, callback=function(k){return true;})
{
	var container = document.createElement('div');
	var title = document.createElement('h2');
	var content = document.createElement('p');

	container.className = "modal-container";
	title.className = "modal-title";

	title.className += " modal-title-" + type;

	content.className = "modal-content";

	title.textContent = titleText;
	content.textContent = contentText;

	container.appendChild(title);
	container.appendChild(content);

	if(okCancel)
	{
		var buttons = document.createElement('div');
		var ok = document.createElement('button');
		var cancel = document.createElement('button');

		ok.textContent = "OK";
		cancel.textContent = "Cancel";

		ok.addEventListener("click", function()
		{
			cancel.disabled = true;
			ok.disabled = true;

			callback(true);

			document.body.removeChild(container);
		});

		cancel.addEventListener("click", function()
		{
			cancel.disabled = true;
			ok.disabled = true;

			callback(false);

			document.body.removeChild(container);
		});

		buttons.appendChild(ok);
		buttons.appendChild(cancel);

		container.appendChild(buttons);

		container.className += " modal-container-hidden";
		buttons.className = "modal-buttons";
		ok.className = "btn-primary";
	}

	document.body.appendChild(container);

	if(okCancel)
	{
		setTimeout(() => container.className += " modal-container-unhide modal-container-move", 100);
	}
	else
	{
		setTimeout(() => container.className += " modal-container-hide modal-container-move", 100);
		setTimeout(() => document.body.removeChild(container), 2000);
	}
}

function setStatusUI(uiText, uiBtn, status)
{
	uiText.textContent = status;
	uiText.className = status.toLowerCase();

	if(status === "Running")
		uiBtn.textContent = "Stop";
	else
		uiBtn.textContent = "Start";
}

function loadTemplate(path, domElem)
{
	var xhr = new XMLHttpRequest();
	xhr.open('GET', chrome.extension.getURL(path), true);
	xhr.onreadystatechange = function()
	{
		if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200)
			domElem.innerHTML = xhr.responseText;
	};
	xhr.send(null);
}

document.addEventListener('DOMContentLoaded', function()
{
	var textBanned = document.getElementById('text-banned');
	var btnSaveBanned = document.getElementById('btn-save-banned');
	var btnDiscardBanned = document.getElementById('btn-discard-banned');

	var btnDonate = document.getElementById('btn-donate');

	var btnToggleStatus = document.getElementById('btn-toggle-status');
	var textStatus = document.getElementById('text-status');

	var dropTimeFrom = document.getElementById('drop-time-from');
	var dropTimeTo = document.getElementById('drop-time-to');

	var checkMon = document.getElementById('check-mon');
	var checkTue = document.getElementById('check-tue');
	var checkWed = document.getElementById('check-wed');
	var checkThu = document.getElementById('check-thu');
	var checkFri = document.getElementById('check-fri');
	var checkSat = document.getElementById('check-sat');
	var checkSun = document.getElementById('check-sun');

	var btnSaveSchedule =  document.getElementById('btn-save-schedule');
	var btnDiscardSchedule =  document.getElementById('btn-discard-schedule');

	var version =  document.getElementById('version');
	var changelog =  document.getElementById('changelog');
	var about =  document.getElementById('about');

	version.textContent = chrome.runtime.getManifest().version;
	loadTemplate("template/changelog.html", changelog);
	loadTemplate("template/about.html", about);

	for (var h = 0; h < 24; h++)
	{
		for (var m = 0; m <= 50; m += 15)
		{
			var option1 = document.createElement("option");
			var option2 = document.createElement("option");
			option1.value = [h, m];
			option2.value = [h, m];

			var hour = h;
			var min = m;

			if(hour < 10)
				hour = "0" + hour;
			if(min < 10)
				min = "0" + min;

			option1.text = hour + ":" + min;
			option2.text = hour + ":" + min;

			dropTimeFrom.add(option1);
			dropTimeTo.add(option2);
		}
	}

	btnDonate.addEventListener("click", function() { chrome.runtime.sendMessage({type:"donate"}); });

	chrome.storage.local.get({scheduleTime:[[0, 0], [0, 0]], scheduleWeek:[true, true, true, true, true, true, true]}, function(result)
	{
		timeFrom = result.scheduleTime[0];
		timeTo = result.scheduleTime[1];

		week = result.scheduleWeek;

		dropTimeFrom.value = timeFrom;
		dropTimeTo.value = timeTo;

		checkMon.checked = week[0]
		checkTue.checked = week[1]
		checkWed.checked = week[2]
		checkThu.checked = week[3]
		checkFri.checked = week[4]
		checkSat.checked = week[5]
		checkSun.checked = week[6]
	});

	btnSaveSchedule.addEventListener("click", function()
	{
		var from = dropTimeFrom.value.split(',');
		var to = dropTimeTo.value.split(',');

		time = [from, to];

		week =
		[
			checkMon.checked,
			checkTue.checked,
			checkWed.checked,
			checkThu.checked,
			checkFri.checked,
			checkSat.checked,
			checkSun.checked
		]

		chrome.storage.local.set({scheduleTime:time, scheduleWeek:week}, function(result)
		{
			if(chrome.runtime.lastError)
			{
				spawnModal("Error!", "Could not save the schedule.\n" + chrome.runtime.lastError, "failure");
				console.log("Error: could not save the schedule.");
				console.log(chrome.runtime.lastError);
				return;
			}

			spawnModal("Saved!", "Your schedule is saved!", "success");
		});
	});

	btnDiscardSchedule.addEventListener("click", function()
	{
		spawnModal("Warning", "Are you sure you want to discard changes?", "warn", true, function(k)
		{
			if(k)
			{
				chrome.storage.local.get({scheduleTime:[[0, 0], [0, 0]], scheduleWeek:[true, true, true, true, true, true, true]}, function(result)
				{
					timeFrom = result.scheduleTime[0];
					timeTo = result.scheduleTime[1];
			
					week = result.scheduleWeek;
			
					dropTimeFrom.value = timeFrom;
					dropTimeTo.value = timeTo;
			
					checkMon.checked = week[0]
					checkTue.checked = week[1]
					checkWed.checked = week[2]
					checkThu.checked = week[3]
					checkFri.checked = week[4]
					checkSat.checked = week[5]
					checkSun.checked = week[6]
				});
			}
		});
	});

	chrome.storage.local.get({status:"Running"}, function(result)
	{
		status = result.status;

		setStatusUI(textStatus, btnToggleStatus, status);
	});

	btnToggleStatus.addEventListener("click", function()
	{
		chrome.storage.local.get({status:"Running"}, function(result)
		{
			status = result.status;

			if(status === "Running")
			{
				chrome.storage.local.set({ status:"Stopped" }, function()
				{
					if(chrome.runtime.lastError)
					{
						spawnModal("Error!", "Could not stop the service.\n" + chrome.runtime.lastError, "failure");
						console.log("Error: could not stop the service.");
						console.log(chrome.runtime.lastError);
						return;
					}

					setStatusUI(textStatus, btnToggleStatus, "Stopped");
				});
			}
			else
			{
				chrome.storage.local.set({ status:"Running" }, function()
				{
					if(chrome.runtime.lastError)
					{
						spawnModal("Error!", "Could not save the service.\n" + chrome.runtime.lastError, "failure");
						console.log("Error: could not start the service.");
						console.log(chrome.runtime.lastError);
						return;
					}

					setStatusUI(textStatus, btnToggleStatus, "Running");
				});
			}
		});
	});

	chrome.storage.local.get({ bannedDomains: [] }, function(result)
	{
		var bannedDomains = result.bannedDomains;
		textBanned.value = "";
		bannedDomains.forEach(x => textBanned.value += x + "\n");
	});

	btnSaveBanned.addEventListener("click", function()
	{
		var domains = textBanned.value.split("\n");
		textBanned.value = "";
		var bannedDomains = [];

		for (var i = 0; i < domains.length; i++)
		{
			if(domains[i] === "" && domains[i] != undefined && domains[i] != null)
				continue;

			if(domains[i].includes("http://") || domains[i].includes("https://"))
				domains[i] = helper.extractDomain(domains[i]);

			var www = "";
			if(domains[i].includes("www."))
				www = domains[i].split("www.")[1];
			else
				www = "www." + domains[i];

			if(helper.extractElement(bannedDomains, x => x === www) === null)
				bannedDomains.push(www);

			if(helper.extractElement(bannedDomains, x => x === domains[i]) === null)
				bannedDomains.push(domains[i]);
		}

		bannedDomains.sort();

		chrome.storage.local.set({ bannedDomains: bannedDomains }, function()
		{
			if(chrome.runtime.lastError)
			{
				spawnModal("Error!", "Could not update banned domains.\n" + chrome.runtime.lastError, "failure");
				console.log("Error: could not update banned domains.");
				console.log(chrome.runtime.lastError);
				return;
			}

			textBanned.value = "";
			bannedDomains.forEach(x => textBanned.value += x + "\n");
			spawnModal("Saved!", "Your banned domain list was updated.", "success");
		});
	});

	btnDiscardBanned.addEventListener("click", function()
	{
		spawnModal("Warning", "Are you sure you want to discard changes?", "warn", true, function(k)
		{
			if(k)
			{
				chrome.storage.local.get({ bannedDomains: [] }, function(result)
				{
					var bannedDomains = result.bannedDomains;
					textBanned.value = "";
					bannedDomains.forEach(x => textBanned.value += x + "\n");
				});
			}
		});
	});
});